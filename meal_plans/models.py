from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from django.conf import settings
USER_MODEL = settings.AUTH_USER_MODEL

class MealPlan(models.Model):
    name = models.CharField(max_length=120)
    date = models.DateField()
    owner = models.ForeignKey(
        USER_MODEL, on_delete=models.CASCADE, null=True)
    recipes = models.ManyToManyField("recipes.Recipe",related_name="meal_plans", null=True)
    

    def __str__(self):
        return str(self.name) + " by " + str(self.recipes)